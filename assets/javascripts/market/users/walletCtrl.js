angular.module('market').controller('WalletCtrl', [
  '$scope', '$http', '$cookieStore', '$state',
  function($scope, $http, $cookieStore, $state) {
    var token = $cookieStore.get('btc-market_token');

    $http.get('http://btc-market.herokuapp.com/v1/users/wallet', {
      headers: { 'Authorization': 'Token token=' + token }
    })
    .success(function(data, status, headers, config) {
      $scope.user = data.data.attributes;
    })
    .error(function(data, status, headers, config) {
      console.log('Must login.');
    });

  }
]);
