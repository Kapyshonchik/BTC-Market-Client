angular.module('market').controller('MarketStatusCtrl', [
  '$scope', '$http',
  function($scope, $http) {
    $http.get('http://btc-market.herokuapp.com/v1/markets/status', {})
    .success(function(data, status, headers, config) {
      $scope.market_status = data.data.attributes;
    })
    .error(function(data, status, headers, config) {
    });
  }
]);
