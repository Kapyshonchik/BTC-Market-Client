angular.module('market', ['ui.router', 'ngCookies', 'ui.gravatar', 'chart.js']).config([
  '$stateProvider', '$urlRouterProvider',
  function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/home',
        name: 'home',
        templateUrl: 'assets/javascripts/market/home.html'
      })
      .state('sign_up', {
        url: '/sign_up',
        name: 'sign_up',
        templateUrl: 'assets/javascripts/market/users/templates/sign_up.html'
      })
      .state('login', {
        url: '/login',
        name: 'login',
        templateUrl: 'assets/javascripts/market/users/templates/login.html'
      })
      .state('logout', {
        url: '/logout',
        name: 'logout',
        controller: 'LogoutCtrl'
      })
      .state('profile', {
        url: '/profile',
        name: 'profile',
        templateUrl: 'assets/javascripts/market/users/templates/profile.html'
      })
      .state('history', {
        url: '/profile/history',
        name: 'history',
        templateUrl: 'assets/javascripts/market/histories/history.html'
      })
      .state('notifications', {
        url: '/notifications',
        name: 'notifications',
        templateUrl: 'assets/javascripts/market/notifications/templates/notifications.html'
      })
      .state('new_order', {
        url: '/orders/new',
        name: 'new_order',
        templateUrl: 'assets/javascripts/market/orders/templates/form.html'
      });

    $urlRouterProvider.otherwise('home');
  }
]);
