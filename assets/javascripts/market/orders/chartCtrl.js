angular.module('market').controller('ChartCtrl', [
  '$scope', '$http',
  function($scope, $http) {
    $http.get('http://btc-market.herokuapp.com/v1/graphs/monthly')
    .success(function(data, status, headers, config) {
      $scope.days = data['price_change_data'];
      $scope.labels = [];
      $scope.data   = [];

      angular.forEach($scope.days, function(value) {
        $scope.labels.push(value['created_at']);
        $scope.data.push(parseFloat(value['price']));
      });
    })
    .error(function(data) { console.log('Couldn\'t get graph datas.'); });
  }
]);
