angular.module('market').controller('OrdersCtrl', [
  '$scope', '$http',
  function($scope, $http) {
    $scope.orders = [];

    $http.get('http://btc-market.herokuapp.com/v1/orders', {})
    .success(
      function(data, status, headers, config) {
        $scope.orders = data.data;
      }).error( function(response) { console.log("Some error happend while loading orders."); });
  }
]);
