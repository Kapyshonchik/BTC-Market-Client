angular.module('market').controller('TransactionHistoryCtrl', [
  '$scope', '$cookieStore', '$state', '$http',
  function($scope, $cookieStore, $state, $http) {
    var token = $cookieStore.get('btc-market_token');

    $scope.transactions = [];

    $http.get('http://btc-market.herokuapp.com/v1/histories/transactions',
    {
      headers: { 'Authorization': 'Token token=' + token }
    })
    .success(
      function(data, status, headers, config) {
        $scope.transactions = data.data;
      })
    .error( function(response) { console.log("Some error happend while loading transactions."); });

  }
]);
