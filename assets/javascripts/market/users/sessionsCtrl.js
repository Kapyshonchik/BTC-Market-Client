angular.module('market').controller('SessionsCtrl', [
  '$scope', '$http', '$cookieStore', '$state', '$rootScope',
  function($scope, $http, $cookieStore, $state, $rootScope) {
    $scope.login = function(){
      $http.post('http://btc-market.herokuapp.com/v1/tokens', { "data": { "attributes": $scope.user } })
       .success(function(data, status, headers, config) {
        $cookieStore.put('btc-market_token', data.data.attributes['token']);

        $rootScope.alert = 'Başarı ile giriş yapıldı. Hoş geldiniz...';
        $state.go('home');
      }).error( function(data, status, headers, config) {
        $scope.errors = data['errors'];
      });
    }
  }
]);

angular.module('market').controller('LogoutCtrl', [
  '$scope', '$cookieStore', '$state',
  function($scope, $cookieStore, $state) {
    $cookieStore.remove('btc-market_token');
    $state.go('home');
  }
]);
