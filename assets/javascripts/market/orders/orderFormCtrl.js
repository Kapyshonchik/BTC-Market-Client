angular.module('market').controller('OrderFormCtrl', [
  '$scope', '$http', '$cookieStore', '$state', '$rootScope',
  function($scope, $http, $cookieStore, $state, $rootScope) {
    var token = $cookieStore.get('btc-market_token');

    $scope.sendForm = function() {
      $http.post('http://btc-market.herokuapp.com/v1/orders', { 'data': { 'attributes': $scope.order } },
        { headers: { 'Authorization': 'Token token=' + token } }
      )
      .success(function(data, status, headers, config) {
        $rootScope.alert = 'Emriniz başarıyla eklendi.';
        $state.go('home');
      })
      .error(function(data, status, headers, config) {
        console.log('Error happend.');
        $scope.errors = data['errors'];
      })

    }
  }
]);
