angular.module('market').controller('UsersCtrl', [
  '$scope', '$http', '$state', '$rootScope', '$cookieStore',
  function($scope, $http, $state, $rootScope, $cookieStore) {
    $scope.addUser = function(){
      $http.post('http://btc-market.herokuapp.com/v1/users', {"data": { "attributes": $scope.user }})
       .success(function(data, status, headers, config) {
        $cookieStore.put('btc-market_token', data.data.attributes['token']);
        $rootScope.alert = 'Kayıt olduğunuz için teşekkürler. Bakiye hesabınıza aktarıldı';

        $state.go('home');
      }).error( function(data, status, headers, config) {
        $scope.errors = data['errors'];
      });
    }
  }
]);

angular.module('market').controller('ProfileCtrl', [
  '$scope', '$http', '$cookieStore', '$state',
  function($scope, $http, $cookieStore, $state) {
    var token = $cookieStore.get('btc-market_token');

    $http.get('http://btc-market.herokuapp.com/v1/users/me', {
      headers: { 'Authorization': 'Token token=' + token }
    })
    .success(function(data, status, headers, config) {
      $scope.user = data.data;
    })
    .error(function(data, status, headers, config) {
      console.log('Must login');
      $state.go('login');
    });

  }
]);
