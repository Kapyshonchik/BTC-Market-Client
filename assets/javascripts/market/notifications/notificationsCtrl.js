angular.module('market').controller('NotificationCountCtrl', [
  '$scope', '$http', '$cookieStore',
  function($scope, $http, $cookieStore) {
    var token = $cookieStore.get('btc-market_token');

    $scope.notification_count = 0;

    $http.get('http://btc-market.herokuapp.com/v1/notifications', {
      headers: { 'Authorization': 'Token token=' + token }
    })
    .success(
      function(data, status, headers, config) {
        $scope.notification_count = data.data.length;
    })
    .error( function(response) { console.log("Some error happend while loading notifications."); });
  }
]);


angular.module('market').controller('NotificationsCtrl', [
  '$scope', '$http', '$cookieStore',
  function($scope, $http, $cookieStore) {
    var token = $cookieStore.get('btc-market_token');

    $scope.notifications = [];

    $http.get('http://btc-market.herokuapp.com/v1/notifications', {
      headers: { 'Authorization': 'Token token=' + token }
    })
    .success(
      function(data, status, headers, config) {
        $scope.notifications = data.data;
      }
    )
    .error(
      function(response) { console.log('Some error happend while loading notifications.') }
    );
  }
]);
